package densh.com.gdgfirebasedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

public class AddTaskActivity extends AppCompatActivity {

    EditText mEditText;
    EditText mDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        mEditText = (EditText) findViewById(R.id.editTextName);
        mDescription = (EditText) findViewById(R.id.editTextDescription);

        FirebaseDatabase database = FirebaseDatabase.getInstance();

        final DatabaseReference reference = database.getReference();
        findViewById(R.id.addTask).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Task task = new Task(UUID.randomUUID().toString(), mEditText.getText().toString(), mDescription.getText().toString(), false);
                reference.child(Task.COLLECTION_NAME).push().setValue(task);
                finish();
            }
        });
        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
