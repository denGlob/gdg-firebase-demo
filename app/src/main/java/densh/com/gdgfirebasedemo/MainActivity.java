package densh.com.gdgfirebasedemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private FirebaseRecyclerAdapter mAdapter;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddTaskActivity.class);
                startActivity(intent);
            }
        });

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        reference = database.getReference();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new FirebaseRecyclerAdapter<Task, TaskHolder>(Task.class, R.layout.task_list_item, TaskHolder.class, reference.child("tasks")) {

            @Override
            public void onViewRecycled(TaskHolder holder) {
                holder.setOnCheckboxClick(null);
                super.onViewRecycled(holder);
            }

            @Override
            protected void populateViewHolder(TaskHolder viewHolder, final Task model, int position) {
                viewHolder.setName(model.getName());
                viewHolder.setDescription(model.getDescription());
                viewHolder.setIsDone(model.isDone());
                viewHolder.setOnCheckboxClick(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                        final Query query = reference.child("tasks").orderByChild("id").equalTo(model.getId());
                        query.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                DataSnapshot nodeDataSnapshot = dataSnapshot.getChildren().iterator().next();
                                String key = nodeDataSnapshot.getKey();
                                String path = "/" + dataSnapshot.getKey() + "/" + key;

                                HashMap<String, Object> result = new HashMap<>();
                                result.put("done", b);
                                reference.child(path).updateChildren(result).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<Void> task) {
                                        Snackbar.make(fab, "Saved", Snackbar.LENGTH_SHORT).show();
                                    }
                                });
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                //
                            }
                        });
                    }
                });
            }
        };

        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.cleanup();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class TaskHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView fieldName;
        TextView fieldDescription;
        CheckBox checkBox;

        public TaskHolder(View itemView) {
            super(itemView);
            mView = itemView;
            fieldName = (TextView) mView.findViewById(R.id.textViewName);
            fieldDescription = (TextView) mView.findViewById(R.id.textViewDescription);
            checkBox = (CheckBox) mView.findViewById(R.id.checkBoxIsDone);
        }

        public void setName(String name) {
            fieldName.setText(name);
        }

        public void setDescription(String description) {
            fieldDescription.setText(description);
        }

        public void setOnCheckboxClick(CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
            checkBox.setOnCheckedChangeListener(onCheckedChangeListener);
        }

        public void setIsDone(@NonNull Boolean isDone) {
            checkBox.setChecked(isDone);
        }
    }
}
